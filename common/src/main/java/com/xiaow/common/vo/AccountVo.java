package com.xiaow.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AccountVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String password;

    private String username;

    private String email;

    private String info;

    private List<String> urls;

}

package com.xiaow.account.utils;

import com.xiaow.account.entity.Urlandperm;

import java.util.LinkedList;
import java.util.List;

public class SecurityUtil {

    public static List<String> generateUrls(List<Urlandperm> list) {
        List<String> urls = new LinkedList<>();
        for (Urlandperm urlandperm : list) {
            urls.add(urlandperm.getUrl());
        }
        return urls;
    }
}

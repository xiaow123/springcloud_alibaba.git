package com.xiaow.account.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaow.account.entity.Urlandperm;
import com.xiaow.account.service.UrlandpermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 路径和权限对应表 前端控制器
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@RestController
@RequestMapping("/urlandperm")
public class UrlandpermController {
    @Autowired
    private UrlandpermService urlandpermService;

    @GetMapping("/list")
    public List list() {
        return urlandpermService.list();
    }

    @GetMapping("/getNoAuthUrls")
    public List getNoAuthUrls() {
        QueryWrapper<Urlandperm> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("permid", -1);
        List<String> all = new LinkedList<>();
        for (Urlandperm urlandperm : urlandpermService.list(objectQueryWrapper)) {
            all.add(urlandperm.getUrl());
        }
        return all;
    }

    @GetMapping("/findByAcId")
    List<Urlandperm> findByAcId(Integer accountid) {
        return urlandpermService.findByAcId(accountid);
    }


}

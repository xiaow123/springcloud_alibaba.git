package com.xiaow.account.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaow.account.entity.Account;
import com.xiaow.account.entity.Urlandperm;
import com.xiaow.account.service.AccountService;
import com.xiaow.account.service.UrlandpermService;
import com.xiaow.account.utils.SecurityUtil;
import com.xiaow.common.utils.JwtUtils;
import com.xiaow.common.utils.MD5Util;
import com.xiaow.common.vo.AccountVo;
import com.xiaow.common.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 账号 前端控制器
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@RestController
@RequestMapping("/account")
public class AccountController {
    @Autowired
    AccountService accountService;
    @Autowired
    UrlandpermService urlandpermService;

    @GetMapping("/list")
    public List list(){
        return accountService.list();
    }


    @GetMapping("/login")
    public Result login(Integer accontid, String password){
        QueryWrapper<Account> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("id",accontid);
        Account one = accountService.getOne(objectQueryWrapper);
        String encode = MD5Util.encode(password);
        if(encode.equals(one.getPassword())){
            List<Urlandperm> byAcId = urlandpermService.findByAcId(accontid);
            List<String> urls = SecurityUtil.generateUrls(byAcId);
            String jwt = JwtUtils.generateJsonWebToken(new AccountVo().setUrls(urls)
                    .setEmail(one.getEmail())
                    .setId(one.getId())
                    .setUsername(one.getUsername())
                    .setInfo(one.getInfo()));
            return Result.result("验证成功",200,jwt);
        }
        return Result.result("出现问题", -1,null);
    }



}

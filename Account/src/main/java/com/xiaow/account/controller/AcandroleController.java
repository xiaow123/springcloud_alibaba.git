package com.xiaow.account.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 账户角色关联表 前端控制器
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@RestController
@RequestMapping("/acandrole")
public class AcandroleController {

}

package com.xiaow.account.mapper;

import com.xiaow.account.entity.Roleandperm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface RoleandpermMapper extends BaseMapper<Roleandperm> {

}

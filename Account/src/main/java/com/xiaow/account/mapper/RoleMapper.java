package com.xiaow.account.mapper;

import com.xiaow.account.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface RoleMapper extends BaseMapper<Role> {

}

package com.xiaow.account.mapper;

import com.xiaow.account.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账号 Mapper 接口
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface AccountMapper extends BaseMapper<Account> {

}

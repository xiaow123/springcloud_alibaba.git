package com.xiaow.account.mapper;

import com.xiaow.account.entity.Urlandperm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 路径和权限对应表 Mapper 接口
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface UrlandpermMapper extends BaseMapper<Urlandperm> {

    List<Urlandperm> findByAcId(Integer accountid);

}

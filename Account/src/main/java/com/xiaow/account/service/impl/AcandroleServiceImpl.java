package com.xiaow.account.service.impl;

import com.xiaow.account.entity.Acandrole;
import com.xiaow.account.mapper.AcandroleMapper;
import com.xiaow.account.service.AcandroleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户角色关联表 服务实现类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@Service
public class AcandroleServiceImpl extends ServiceImpl<AcandroleMapper, Acandrole> implements AcandroleService {

}

package com.xiaow.account.service.impl;

import com.xiaow.account.entity.Urlandperm;
import com.xiaow.account.mapper.UrlandpermMapper;
import com.xiaow.account.service.UrlandpermService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 路径和权限对应表 服务实现类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@Service
public class UrlandpermServiceImpl extends ServiceImpl<UrlandpermMapper, Urlandperm> implements UrlandpermService {

    @Override
    public List<Urlandperm> findByAcId(Integer accountid) {
        return baseMapper.findByAcId(accountid);
    }
}

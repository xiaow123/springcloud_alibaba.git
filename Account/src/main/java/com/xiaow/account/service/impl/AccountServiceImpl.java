package com.xiaow.account.service.impl;

import com.xiaow.account.entity.Account;
import com.xiaow.account.mapper.AccountMapper;
import com.xiaow.account.service.AccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账号 服务实现类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

}

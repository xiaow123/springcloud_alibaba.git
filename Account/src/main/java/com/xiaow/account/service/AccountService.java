package com.xiaow.account.service;

import com.xiaow.account.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账号 服务类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface AccountService extends IService<Account> {

}

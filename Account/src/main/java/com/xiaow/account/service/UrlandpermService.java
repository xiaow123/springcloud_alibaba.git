package com.xiaow.account.service;

import com.xiaow.account.entity.Urlandperm;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 路径和权限对应表 服务类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface UrlandpermService extends IService<Urlandperm> {

    List<Urlandperm> findByAcId(Integer accountid);

}

package com.xiaow.consumer.service;

import com.xiaow.consumer.service.impl.TestServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "provider",fallback = TestServiceImpl.class)
public interface TestSerice {
    @GetMapping("/test")
    public String test();

}

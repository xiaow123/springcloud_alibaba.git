package com.xiaow.consumer.controller;

import com.xiaow.consumer.service.TestSerice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    TestSerice testSerice;

    @GetMapping("/consumertest")
    public String consumertest() {
        return testSerice.test();
    }
}
